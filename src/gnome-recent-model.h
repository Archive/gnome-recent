/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef __GNOME_RECENT_MODEL_H__
#define __GNOME_RECENT_MODEL_H__

#include "gnome-recent-item.h"

G_BEGIN_DECLS

#define GNOME_RECENT_MODEL(obj)		G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_recent_model_get_type (), GnomeRecentModel)
#define GNOME_RECENT_MODEL_CLASS(klass) 	G_TYPE_CHECK_CLASS_CAST (klass, gnome_recent_model_get_type (), GnomeRecentModelClass)
#define GNOME_IS_RECENT_MODEL(obj)		G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_recent_model_get_type ())

typedef struct _GnomeRecentModel GnomeRecentModel;

typedef struct _GnomeRecentModelClass GnomeRecentModelClass;

struct _GnomeRecentModelClass {
	GObjectClass parent_class;
			
	void (*changed) (GnomeRecentModel *model, GList *list);
};

typedef enum {
	GNOME_RECENT_MODEL_SORT_MRU,
	GNOME_RECENT_MODEL_SORT_LRU,
	GNOME_RECENT_MODEL_SORT_NONE
} GnomeRecentModelSort;


GType    gnome_recent_model_get_type     (void);

/* constructors */
GnomeRecentModel * gnome_recent_model_new (GnomeRecentModelSort sort,
					   int limit);

/* public methods */
void     gnome_recent_model_set_filter_mime_types (GnomeRecentModel *model,
						   ...);

void     gnome_recent_model_set_filter_groups (GnomeRecentModel *model, ...);

void     gnome_recent_model_set_filter_uri_schemes (GnomeRecentModel *model,
						   ...);

void     gnome_recent_model_set_sort (GnomeRecentModel *model,
				      GnomeRecentModelSort sort);

gboolean gnome_recent_model_add_full (GnomeRecentModel *model,
				      GnomeRecentItem *item);

gboolean gnome_recent_model_add	     (GnomeRecentModel *model,
				      const gchar *uri);

gboolean gnome_recent_model_delete   (GnomeRecentModel *model,
				      const gchar *uri);

void gnome_recent_model_clear        (GnomeRecentModel *model);

GList * gnome_recent_model_get_list  (GnomeRecentModel *model);

void gnome_recent_model_changed      (GnomeRecentModel *model);

void gnome_recent_model_set_limit    (GnomeRecentModel *model, int limit);
int  gnome_recent_model_get_limit    (GnomeRecentModel *model);


G_END_DECLS

#endif /* __GNOME_RECENT_MODEL_H__ */
