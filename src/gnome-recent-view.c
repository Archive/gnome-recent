/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors:
 *   James Willcox <jwillcox@cs.indiana.edu>
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gtk/gtk.h>
#include "gnome-recent-view.h"


GtkType
gnome_recent_view_get_type (void)
{
	static GtkType view_type = 0;

	if (!view_type)
	{
		static const GTypeInfo view_info =
		{
			sizeof (GnomeRecentViewClass),  /* class_size */
			NULL,			    /* base_init */
			NULL,			    /* base_finalize */
		};

		view_type = g_type_register_static (G_TYPE_INTERFACE,
						    "GnomeRecentView",
						    &view_info, 0);
	}

	return view_type;
}

GnomeRecentModel *
gnome_recent_view_get_model (GnomeRecentView *view)
{
	g_return_val_if_fail (view, NULL);

	return GNOME_RECENT_VIEW_GET_CLASS (view)->do_get_model (view);
}

void
gnome_recent_view_set_model (GnomeRecentView *view, GnomeRecentModel *model)
{
	g_return_if_fail (view);
	g_return_if_fail (model);

	GNOME_RECENT_VIEW_GET_CLASS (view)->do_set_model (view, model);
}
