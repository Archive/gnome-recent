/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors:
 *   James Willcox <jwillcox@cs.indiana.edu>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <libgnomevfs/gnome-vfs.h>
#include <uuid/uuid.h>
#include "gnome-recent-model.h"
#include "gnome-recent-view.h"
#include "gnome-recent-view-gtk.h"
#include "gnome-recent-util.h"
#include "gnome-recent-marshal.h"
#include "gnome-recent-item.h"

struct _GnomeRecentViewGtk {
	GObject parent_instance;	/* We emit signals */

	GtkWidget *menu;
	gchar *start_label;		/* label of the menu we start from */

	gboolean leading_sep;
	gboolean trailing_sep;

	gulong changed_cb_id;

	gchar *uuid;

	GnomeRecentModel *model;
};



struct _GnomeRecentViewGtkMenuData {
	GnomeRecentViewGtk *view;
	GnomeRecentItem *item;
};

typedef struct _GnomeRecentViewGtkMenuData GnomeRecentViewGtkMenuData;

enum {
	ACTIVATE,
	LAST_SIGNAL
};

/* GObject properties */
enum {
	PROP_BOGUS,
	PROP_MENU,
	PROP_MENU_KEY,
};

static guint view_signals[LAST_SIGNAL] = { 0 };


static void
gnome_recent_view_gtk_clear (GnomeRecentViewGtk *view)
{
	GList *menu_children;
	GList *p;
	GObject *menu_item;
	gint *menu_data=NULL;

	g_return_if_fail (view->menu != NULL);

	menu_children = gtk_container_get_children (GTK_CONTAINER (view->menu));

	p = menu_children;
	while (p != NULL) {
		menu_item = (GObject *)p->data;

		menu_data = (gint *)g_object_get_data (menu_item,
						       view->uuid);
	
		if (menu_data) {
			gtk_container_remove (GTK_CONTAINER (view->menu),
					     GTK_WIDGET (menu_item));

		}
		
		p = p->next;
	}
}


static gint
gnome_recent_view_gtk_find_menu_offset_by_label (GnomeRecentViewGtk *view,
					const gchar *text)
{
	gint i;
	GList *menu_children;
	GList *p;
	GtkWidget *menu_item;
	GtkWidget *label;
	const gchar *label_str;
	gint menu_loc=-1;

	g_return_val_if_fail (view, 0);
	if (text == NULL)
		return 0;

	menu_children = GTK_MENU_SHELL (view->menu)->children;

	i = 0;
	p = menu_children;
	while (p != NULL) {
		menu_item = (GtkWidget *)p->data;
		label = GTK_BIN (menu_item)->child;

		if(label != NULL) {
			label_str = gtk_label_get_text (GTK_LABEL (label));

			if (!strcmp (label_str, text)) {
				menu_loc = i;
			}

			break;
		}

		p = p->next;
		i++;
	}

	return menu_loc;
}

static void
gnome_recent_view_gtk_menu_cb (GtkWidget *menu, gpointer data)
{
	gboolean ret;
	GnomeRecentViewGtkMenuData *md = (GnomeRecentViewGtkMenuData *) data;
	GnomeRecentModel *model;
	GnomeRecentItem *item;
	gchar *uri;

	g_return_if_fail (md);
	g_return_if_fail (md->item);
	g_return_if_fail (md->view);
	g_return_if_fail (GNOME_IS_RECENT_VIEW_GTK (md->view));

	uri = gnome_recent_item_get_uri (md->item);
	item = gnome_recent_item_copy (md->item);
	
	ret = FALSE;
	g_signal_emit (G_OBJECT(md->view), view_signals[ACTIVATE], 0,
		       item, &ret);

	gnome_recent_item_free (item);

	if (!ret) {
		model = gnome_recent_view_get_model (GNOME_RECENT_VIEW (md->view));
		gnome_recent_model_delete (model, uri);
	}

	g_free (uri);
}

static void
gnome_recent_view_gtk_destroy_cb (gpointer data, GClosure *closure)
{
	GnomeRecentViewGtkMenuData *md = data;

	gnome_recent_item_free (md->item);
	g_free (md);
}

static GtkWidget *
gnome_recent_view_gtk_new_menu_item (GnomeRecentViewGtk *view,
				     const GnomeRecentItem *item,
				     gint index)
{
	GtkWidget *menu_item;
	GnomeRecentViewGtkMenuData *md=(GnomeRecentViewGtkMenuData *)g_malloc (sizeof (GnomeRecentViewGtkMenuData));
	gchar *text;
	gchar *basename;
	gchar *escaped;
	gchar *uri;

	g_return_val_if_fail (view, NULL);

	if (item != NULL) {

		uri = gnome_recent_item_get_uri_utf8 (item);
		basename = g_path_get_basename (uri);
		escaped = gnome_recent_util_escape_underlines (basename);
		g_free (basename);

		/* avoid having conflicting mnemonics */
		if (index >= 10)
			text = g_strdup_printf ("%d.  %s", index, escaped);
		else
			text = g_strdup_printf ("_%d.  %s", index, escaped);

		g_free (escaped);

		menu_item = gtk_menu_item_new_with_mnemonic (text);


		md->view = view;
		md->item = gnome_recent_item_copy (item);

		g_signal_connect_data (G_OBJECT (menu_item), "activate",
				 G_CALLBACK (gnome_recent_view_gtk_menu_cb),
				 md,
				 (GClosureNotify)gnome_recent_view_gtk_destroy_cb,
				 0);
	}
	else {
		menu_item = gtk_separator_menu_item_new ();
	}
	
	g_return_val_if_fail (menu_item, NULL);

	/**
	 * this is a tag so we can distinguish our menu items
	 * from others that may be in the menu.
	 */
	g_object_set_data (G_OBJECT (menu_item),
			   view->uuid,
			   GINT_TO_POINTER (1));


	gtk_widget_show (menu_item);

	return menu_item;
}

static void
gnome_recent_view_gtk_add_to_menu (GnomeRecentViewGtk *view,
				   const GnomeRecentItem *item,
				   gint display,
				   gint index)
{
	GtkWidget *menu_item;
	gint menu_offset;
	
	g_return_if_fail (view);
	g_return_if_fail (view->menu);

	menu_offset = gnome_recent_view_gtk_find_menu_offset_by_label (view, view->start_label);

	menu_item = gnome_recent_view_gtk_new_menu_item (view, item, display);
	
	gtk_menu_shell_insert (GTK_MENU_SHELL (view->menu), menu_item,
			       menu_offset+index);
}




static void
gnome_recent_view_gtk_set_list (GnomeRecentViewGtk *view, GList *list)
{
	GnomeRecentItem *item;
	GList *p;
	gint display=1;
	gint index=1;

	g_return_if_fail (view);

	gnome_recent_view_gtk_clear (view);

	if (view->leading_sep) {
		gnome_recent_view_gtk_add_to_menu (view, NULL, display, index);
		index++;
	}

	p = list;
	while (p != NULL) {
		item = (GnomeRecentItem *)p->data;

		gnome_recent_view_gtk_add_to_menu (view, item, display, index);

		p = p->next;
		display++;
		index++;
	}

	if (view->trailing_sep)
		gnome_recent_view_gtk_add_to_menu (view, NULL, display, index);
}

static void
model_changed_cb (GnomeRecentModel *model, GList *list, GnomeRecentViewGtk *view)
{
	if (list != NULL)
		gnome_recent_view_gtk_set_list (view, list);
	else
		gnome_recent_view_gtk_clear (view);
}

GnomeRecentModel *
gnome_recent_view_gtk_get_model (GnomeRecentView *view_parent)
{
	GnomeRecentViewGtk *view;
	
	g_return_val_if_fail (view_parent != NULL, NULL);
	view = GNOME_RECENT_VIEW_GTK (view_parent);
	return view->model;
}

void
gnome_recent_view_gtk_set_model (GnomeRecentView *view_parent,
				 GnomeRecentModel *model)
{
	GnomeRecentViewGtk *view;
	
	g_return_if_fail (view_parent != NULL);
	view = GNOME_RECENT_VIEW_GTK (view_parent);

	if (view->model != NULL) {
		g_object_unref (view->model);
		g_signal_handler_disconnect (G_OBJECT (model),
					     view->changed_cb_id);
	}
	
	view->model = model;
	g_object_ref (view->model);

	view->changed_cb_id = g_signal_connect (G_OBJECT (model), "changed",
			  G_CALLBACK (model_changed_cb), view);

	gnome_recent_model_changed (view->model);
}

void
gnome_recent_view_gtk_set_leading_sep (GnomeRecentViewGtk *view, gboolean val)
{
	view->leading_sep = val;

	gnome_recent_view_gtk_clear (view);
	gnome_recent_model_changed (view->model);
}

void
gnome_recent_view_gtk_set_trailing_sep (GnomeRecentViewGtk *view, gboolean val)
{
	view->trailing_sep = val;

	gnome_recent_view_gtk_clear (view);
	gnome_recent_model_changed (view->model);
}

static void
gnome_recent_view_gtk_set_property (GObject *object,
			   guint prop_id,
			   const GValue *value,
			   GParamSpec *pspec)
{
	GnomeRecentViewGtk *view = GNOME_RECENT_VIEW_GTK (object);

	switch (prop_id)
	{
		case PROP_MENU:
			gnome_recent_view_gtk_set_menu (view,
					       GTK_WIDGET (g_value_get_object (value)));
		break;
		case PROP_MENU_KEY:
			gnome_recent_view_gtk_set_start_label (view,
					g_value_get_string (value));
		break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
gnome_recent_view_gtk_get_property (GObject *object,
			   guint prop_id,
			   GValue *value,
			   GParamSpec *pspec)
{
	GnomeRecentViewGtk *view = GNOME_RECENT_VIEW_GTK (object);

	switch (prop_id)
	{
		case PROP_MENU:
			g_value_set_object (value, view->menu);
		break;
		case PROP_MENU_KEY:
			g_value_set_string (value, view->start_label);
		break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
gnome_recent_view_gtk_finalize (GObject *object)
{
	GnomeRecentViewGtk *view = GNOME_RECENT_VIEW_GTK (object);

	g_signal_handler_disconnect (G_OBJECT (view->model),
				     view->changed_cb_id);

	g_free (view->start_label);
	g_free (view->uuid);

	g_object_unref (view->menu);
	g_object_unref (view->model);


}

static void
gnome_recent_view_gtk_class_init (GnomeRecentViewGtkClass * klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);

	object_class->set_property = gnome_recent_view_gtk_set_property;
	object_class->get_property = gnome_recent_view_gtk_get_property;
	object_class->finalize     = gnome_recent_view_gtk_finalize;

	view_signals[ACTIVATE] = g_signal_new ("activate",
			G_OBJECT_CLASS_TYPE (object_class),
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GnomeRecentViewGtkClass, activate),
			NULL, NULL,
			gnome_recent_BOOLEAN__POINTER,
			G_TYPE_BOOLEAN, 1,
			G_TYPE_POINTER);

	g_object_class_install_property (object_class,
					 PROP_MENU,
					 g_param_spec_object ("menu",
						 	      "Menu",
							      "The GtkMenuShell this object will update.",
							      gtk_menu_get_type(),
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_MENU_KEY,
					 g_param_spec_string ("start-label",
						 	      "Start Label",
							      "The label of the menu item that the recent list will start after",
							      "Properties",
							      G_PARAM_READWRITE));


	klass->activate = NULL;
}

static void
gnome_recent_view_init (GnomeRecentViewClass *iface)
{
	iface->do_get_model = gnome_recent_view_gtk_get_model;
	iface->do_set_model = gnome_recent_view_gtk_set_model;
}


static void
gnome_recent_view_gtk_init (GnomeRecentViewGtk * view)
{
	uuid_t uuid;

	view->leading_sep = FALSE;
	view->trailing_sep = FALSE;

	view->uuid = g_new (gchar, 100);
	uuid_generate (uuid);
	uuid_unparse (uuid, view->uuid);
}

/**
 * gnome_recent_view_gtk_set_menu:
 * @view: A GnomeRecentViewGtk object.
 * @menu: The GtkMenuShell to put the menu items in.
 *
 * Use this function to change the GtkMenuShell that the recent
 * documents appear in.
 *
 */
void
gnome_recent_view_gtk_set_menu (GnomeRecentViewGtk *view,
				GtkWidget *menu)
{
	g_return_if_fail (view);
	g_return_if_fail (GNOME_IS_RECENT_VIEW_GTK (view));
	g_return_if_fail (menu);

	if (view->menu != NULL)
		g_object_unref (view->menu);
	
	view->menu = menu;
	g_object_ref (view->menu);
}

/**
 * gnome_recent_view_gtk_set_start_label:
 * @view: A GnomeRecentViewGtk object.
 * @start_label: Where inside the menu the items should go.
 *
 * The menu items will appear after the menu item with the given label
 *
 */
void
gnome_recent_view_gtk_set_start_label (GnomeRecentViewGtk *view,
				    const gchar *start_label)
{
	g_return_if_fail (view);
	g_return_if_fail (GNOME_IS_RECENT_VIEW_GTK (view));
	
	view->start_label = g_strdup (start_label);
}

/**
 * gnome_recent_view_gtk_get_menu:
 * @view: A GnomeRecentViewGtk object.
 *
 */
GtkWidget *
gnome_recent_view_gtk_get_menu (GnomeRecentViewGtk *view)
{
	return view->menu;
}

/**
 * gnome_recent_view_gtk_get_menu:
 * @view: A GnomeRecentViewGtk object.
 *
 */
gchar *
gnome_recent_view_gtk_get_start_label (GnomeRecentViewGtk *view)
{
	return view->start_label;
}


/**
 * gnome_recent_view_gtk_new:
 * @appname: The name of your application.
 * @limit:  The maximum number of items allowed.
 *
 * This creates a new GnomeRecentViewGtk object.
 *
 * Returns: a GnomeRecentViewGtk object
 */
GnomeRecentViewGtk *
gnome_recent_view_gtk_new (GtkWidget *menu, const gchar *start_label)
{
	GnomeRecentViewGtk *view;

	g_return_val_if_fail (menu, NULL);

	view = GNOME_RECENT_VIEW_GTK (g_object_new (gnome_recent_view_gtk_get_type (),
					   "start-label",
					   g_strdup (start_label),
					   "menu",
					   menu, NULL));

	g_return_val_if_fail (view, NULL);
	
	return view;
}

/**
 * gnome_recent_view_gtk_get_type:
 * @:
 *
 * This returns a GType representing a GnomeRecentViewGtk object.
 *
 * Returns: a GType
 */
GType
gnome_recent_view_gtk_get_type (void)
{
	static GType gnome_recent_view_gtk_type = 0;

	if(!gnome_recent_view_gtk_type) {
		static const GTypeInfo gnome_recent_view_gtk_info = {
			sizeof (GnomeRecentViewGtkClass),
			NULL, /* base init */
			NULL, /* base finalize */
			(GClassInitFunc)gnome_recent_view_gtk_class_init, /* class init */
			NULL, /* class finalize */
			NULL, /* class data */
			sizeof (GnomeRecentViewGtk),
			0,
			(GInstanceInitFunc) gnome_recent_view_gtk_init
		};

		static const GInterfaceInfo view_info =
		{
			(GInterfaceInitFunc) gnome_recent_view_init,
			NULL,
			NULL
		};

		gnome_recent_view_gtk_type = g_type_register_static (G_TYPE_OBJECT,
							"GnomeRecentViewGtk",
							&gnome_recent_view_gtk_info, 0);
		g_type_add_interface_static (gnome_recent_view_gtk_type,
					     GNOME_TYPE_RECENT_VIEW,
					     &view_info);
	}

	return gnome_recent_view_gtk_type;
}

