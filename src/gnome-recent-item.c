/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors:
 *   James Willcox <jwillcox@cs.indiana.edu>
 */


#include <stdio.h>
#include <glib.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include "gnome-recent-item.h"
#include "gnome-recent-util.h"



GnomeRecentItem *
gnome_recent_item_new ()
{
	GnomeRecentItem *item;

	item = g_new (GnomeRecentItem, 1);

	item->groups = NULL;
	item->private = FALSE;
	item->uri = NULL;
	item->mime_type = NULL;

	return item;
}

void
gnome_recent_item_free (GnomeRecentItem *item)
{
	if (item->uri)
		g_free (item->uri);

	if (item->mime_type)
		g_free (item->mime_type);

	if (item->groups) {
		g_list_foreach (item->groups, (GFunc)g_free, NULL);
		g_list_free (item->groups);
		item->groups = NULL;
	}

	g_free (item);
}

GnomeRecentItem * 
gnome_recent_item_new_from_uri (const gchar *uri)
{
	GnomeRecentItem *item;

	g_return_val_if_fail (uri != NULL, NULL);

	item = gnome_recent_item_new ();
	item->uri = g_strdup (uri);
	item->mime_type = gnome_vfs_get_mime_type (uri);

	if (!item->mime_type)
		item->mime_type = g_strdup (GNOME_VFS_MIME_TYPE_UNKNOWN);

	return item;
}

static GList *
gnome_recent_item_copy_groups (const GList *list)
{
	GList *newlist = NULL;

	while (list) {
		gchar *group = (gchar *)list->data;

		newlist = g_list_prepend (newlist, g_strdup (group));

		list = list->next;
	}

	return newlist;
}


GnomeRecentItem *
gnome_recent_item_copy (const GnomeRecentItem *item)
{
	GnomeRecentItem *newitem;

	newitem = gnome_recent_item_new ();
	newitem->uri = g_strdup (item->uri);
	if (item->mime_type)
		newitem->mime_type = g_strdup (item->mime_type);
	newitem->timestamp = item->timestamp;
	newitem->private = item->private;
	newitem->groups = gnome_recent_item_copy_groups (item->groups);

	return newitem;
}

/*
GnomeRecentItem *
gnome_recent_item_new_valist (const gchar *uri, va_list args)
{
	GnomeRecentItem *item;
	GnomeRecentArg arg;
	gchar *str1;
	gchar *str2;
	gboolean priv;

	item = gnome_recent_item_new ();

	arg = va_arg (args, GnomeRecentArg);

	while (arg != GNOME_RECENT_ARG_NONE) {
		switch (arg) {
			case GNOME_RECENT_ARG_MIME_TYPE:
				str1 = va_arg (args, gchar*);

				gnome_recent_item_set_mime_type (item, str1);
			break;
			case GNOME_RECENT_ARG_GROUP:
				str1 = va_arg (args, gchar*);

				gnome_recent_item_add_group (item, str1);
			break;
			case GNOME_RECENT_ARG_PRIVATE:
				priv = va_arg (args, gboolean);

				gnome_recent_item_set_private (item, priv);
			break;
			default:
			break;
		}

		arg = va_arg (args, GnomeRecentArg);
	}

	return item;
}
*/

void 
gnome_recent_item_set_uri (GnomeRecentItem *item, const gchar *uri)
{
	gchar *ascii_uri;

	ascii_uri = g_filename_from_utf8 (uri, -1, NULL, NULL, NULL);

	g_return_if_fail (ascii_uri != NULL);
	
	item->uri = ascii_uri;
}

gchar * 
gnome_recent_item_get_uri (const GnomeRecentItem *item)
{
	return g_strdup (item->uri);
}

gchar *
gnome_recent_item_get_uri_utf8 (const GnomeRecentItem *item)
{
	return g_filename_to_utf8 (item->uri, -1, NULL, NULL, NULL);
}

void 
gnome_recent_item_set_mime_type (GnomeRecentItem *item, const gchar *mime)
{
	item->mime_type = g_strdup (mime);
}

gchar * 
gnome_recent_item_get_mime_type (const GnomeRecentItem *item)
{
	return g_strdup (item->mime_type);
}

void 
gnome_recent_item_set_timestamp (GnomeRecentItem *item, time_t timestamp)
{
	item->timestamp = timestamp;
}

time_t 
gnome_recent_item_get_timestamp (const GnomeRecentItem *item)
{
	return item->timestamp;
}

const GList *
gnome_recent_item_get_groups (const GnomeRecentItem *item)
{
	return item->groups;
}

gboolean
gnome_recent_item_in_group (const GnomeRecentItem *item, const gchar *group_name)
{
	GList *tmp;

	tmp = item->groups;
	while (tmp != NULL) {
		gchar *val = (gchar *)tmp->data;
		
		if (strcmp (group_name, val) == 0)
			return TRUE;

		tmp = tmp->next;
	}
	
	return FALSE;
}

void
gnome_recent_item_add_group (GnomeRecentItem *item, const gchar *group_name)
{
	g_return_if_fail (group_name != NULL);
	
	item->groups = g_list_append (item->groups, g_strdup (group_name));
}

void
gnome_recent_item_remove_group (GnomeRecentItem *item, const gchar *group_name)
{
	GList *tmp;

	g_return_if_fail (group_name != NULL);

	tmp = item->groups;
	while (tmp != NULL) {
		gchar *val = (gchar *)tmp->data;
		
		if (strcmp (group_name, val) == 0) {
			item->groups = g_list_remove (item->groups,
						      val);
			g_free (val);
			break;
		}

		tmp = tmp->next;
	}
}

void
gnome_recent_item_set_private (GnomeRecentItem *item, gboolean priv)
{
	item->private = priv;
}

gboolean
gnome_recent_item_get_private (const GnomeRecentItem *item)
{
	return item->private;
}
