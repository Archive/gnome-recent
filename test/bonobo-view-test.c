/*
 * hello.c
 *
 * A hello world application using the Bonobo UI handler
 *
 * Authors:
 *	Michael Meeks    <michael@ximian.com>
 *	Murray Cumming   <murrayc@usa.net>
 *      Havoc Pennington <hp@redhat.com>
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *               2001 Murray Cumming,
 *               2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <stdlib.h>

#include <libbonoboui.h>
#include <glade/glade-xml.h>
#include <libgnomevfs/gnome-vfs.h>
#include "gnome-recent.h"

/* Keep a list of all open application windows */
static GSList *app_list = NULL;

/* the GnomeRecentModel object */
static GnomeRecentModel *model;
static GnomeRecentModel *global_model;

static GnomeRecentViewBonobo *view;
static GnomeRecentViewBonobo *global_view;

#define GNOME_RECENT_UI_XML "gnome-recent-test.xml"

/*   A single forward prototype - try and keep these
 * to a minumum by ordering your code nicely */
static GtkWidget *hello_new (void);

static void
hello_on_menu_file_new (BonoboUIComponent *uic,
			gpointer           user_data,
			const gchar       *verbname)
{
	gtk_widget_show_all (hello_new ());
}

static void
show_nothing_dialog(GtkWidget* parent)
{
	GtkWidget* dialog;

	dialog = gtk_message_dialog_new (
		GTK_WINDOW (parent),
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
		_("This does nothing; it is only a demonstration."));

	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
hello_on_menu_file_open (BonoboUIComponent *uic,
			    gpointer           user_data,
			    const gchar       *verbname)
{
	gchar *uri;

	uri = bonobo_file_selector_open (NULL, TRUE, "Open....", NULL, NULL);

	if (uri != NULL) {
		GnomeRecentItem *item = gnome_recent_item_new_from_uri (uri);
		gnome_recent_item_add_group (item, "Bonobo Test");
		gnome_recent_model_add_full (model, item);
	}
}

static void
hello_on_menu_file_save (BonoboUIComponent *uic,
			    gpointer           user_data,
			    const gchar       *verbname)
{
	show_nothing_dialog (GTK_WIDGET (user_data));
}

static void
hello_on_menu_file_saveas (BonoboUIComponent *uic,
			      gpointer           user_data,
			      const gchar       *verbname)
{
	show_nothing_dialog (GTK_WIDGET (user_data));
}

static void
hello_on_menu_file_exit (BonoboUIComponent *uic,
			    gpointer           user_data,
			    const gchar       *verbname)
{
	g_object_unref (view);
	g_object_unref (global_view);

	
	exit (0);
}	

static void
hello_on_menu_file_close (BonoboUIComponent *uic,
			     gpointer           user_data,
			     const gchar       *verbname)
{
	GtkWidget *app = user_data;

	/* Remove instance: */
	app_list = g_slist_remove (app_list, app);

	gtk_widget_destroy (app);

	if (!app_list)
		hello_on_menu_file_exit(uic, user_data, verbname);
}

static void
hello_on_menu_edit_undo (BonoboUIComponent *uic,
			    gpointer           user_data,
			    const gchar       *verbname)
{
	show_nothing_dialog (GTK_WIDGET (user_data));
}	

static void
hello_on_menu_edit_redo (BonoboUIComponent *uic,
			    gpointer           user_data,
			    const gchar       *verbname)
{
	show_nothing_dialog (GTK_WIDGET (user_data));
}	

static void
hello_on_menu_edit_clear (BonoboUIComponent *uic,
			    gpointer           user_data,
			    const gchar       *verbname)
{
	gnome_recent_model_clear (model);
}

static void
local_clear_cb (GtkButton *button, gpointer data)
{

	gnome_recent_model_clear (model);
}

static void
global_clear_cb (GtkButton *button, gpointer data)
{
	gnome_recent_model_clear (global_model);
}

static void
local_limit_activate_cb (GtkEntry *entry, gpointer data)
{
	const gchar *text;
	gint limit;

	text = gtk_entry_get_text (GTK_ENTRY (entry));
	limit = atoi (text);

	g_print ("Setting limit: %d\n", limit);

	gnome_recent_model_set_limit (model, limit);
}

static void
global_limit_activate_cb (GtkEntry *entry, gpointer data)
{
	const gchar *text;
	gint limit;

	text = gtk_entry_get_text (GTK_ENTRY (entry));
	limit = atoi (text);

	g_print ("Setting limit: %d\n", limit);

	gnome_recent_model_set_limit (global_model, limit);
}

static void
hello_on_menu_help_about (BonoboUIComponent *uic,
			     gpointer           user_data,
			     const gchar       *verbname)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (
		GTK_WINDOW (user_data),
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
		_("BonoboUI-Hello."));

	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

/*
 *   These verb names are standard, see libonobobui/doc/std-ui.xml
 * to find a list of standard verb names.
 *   The menu items are specified in Bonobo_Sample_Hello.xml and
 * given names which map to these verbs here.
 */
static const BonoboUIVerb hello_verbs [] = {
	BONOBO_UI_VERB ("FileNew",    hello_on_menu_file_new),
	BONOBO_UI_VERB ("FileOpen",   hello_on_menu_file_open),
	BONOBO_UI_VERB ("FileSave",   hello_on_menu_file_save),
	BONOBO_UI_VERB ("FileSaveAs", hello_on_menu_file_saveas),
	BONOBO_UI_VERB ("FileClose",  hello_on_menu_file_close),
	BONOBO_UI_VERB ("FileExit",   hello_on_menu_file_exit),

	BONOBO_UI_VERB ("EditUndo",   hello_on_menu_edit_undo),
	BONOBO_UI_VERB ("EditRedo",   hello_on_menu_edit_redo),
	BONOBO_UI_VERB ("EditClear",   hello_on_menu_edit_clear),

	BONOBO_UI_VERB ("HelpAbout",  hello_on_menu_help_about),

	BONOBO_UI_VERB_END
};

static gboolean
open_recent_cb (GtkWidget *widget, const GnomeRecentItem *item, gpointer data)
{
	GnomeVFSURI *vfs_uri;
	gchar *uri;
	gboolean ret;

	uri = gnome_recent_item_get_uri (item);

	g_return_val_if_fail (uri != NULL, FALSE);
	
	/* beware, this doesn't appear to work for mailto: URIs at least, so
	 * this test app is a bit broken
	 */
	vfs_uri = gnome_vfs_uri_new (uri);

	g_return_val_if_fail (vfs_uri != NULL, FALSE);
	
	g_print ("Opening: %s\n", uri);

	if (gnome_vfs_uri_exists (vfs_uri)) {
		GnomeRecentItem *item = gnome_recent_item_new_from_uri (uri);
		gnome_recent_item_add_group (item, "Bonobo Test");
		gnome_recent_model_add_full (model, item);
		ret = TRUE;
	} else  {
		g_print ("Opening failed: %s\n", uri);
		ret = FALSE;
	}

	gnome_vfs_uri_unref (vfs_uri);
	g_free (uri);

	return ret;
}

static BonoboWindow *
hello_create_main_window (void)
{
	BonoboWindow      *win;
	BonoboUIContainer *ui_container;
	BonoboUIComponent *ui_component;

	win = BONOBO_WINDOW (bonobo_window_new ("gnome-recent", _("Gnome Hello")));

	/* Create Container: */
	ui_container = bonobo_window_get_ui_container (win);

	/* This determines where the UI configuration info. will be stored */
	bonobo_ui_engine_config_set_path (bonobo_window_get_ui_engine (win),
					  "/hello-app/UIConfig/kvps");

	/* Create a UI component with which to communicate with the window */
	ui_component = bonobo_ui_component_new_default ();

	/* Associate the BonoboUIComponent with the container */
	bonobo_ui_component_set_container (
		ui_component, BONOBO_OBJREF (ui_container), NULL);

	/* NB. this creates a relative file name from the current dir,
	 * in production you want to pass the application's datadir
	 * see Makefile.am to see how HELLO_SRCDIR gets set. */
	bonobo_ui_util_set_ui (ui_component, "", /* data dir */
			       "./" GNOME_RECENT_UI_XML,
			       "bonobo-hello", NULL);

	/* Associate our verb -> callback mapping with the BonoboWindow */
	/* All the callback's user_data pointers will be set to 'win' */
	bonobo_ui_component_add_verb_list_with_data (ui_component, hello_verbs, win);

	/* Setup the GnomeRecentModel stuff */
	model = gnome_recent_model_new (GNOME_RECENT_MODEL_SORT_MRU, 20);
	gnome_recent_model_set_filter_groups (model, "Bonobo Test", NULL);

	/* try these if you want
	gnome_recent_model_set_filter_mime_types (model, "text/plain", NULL);
	gnome_recent_model_set_filter_uri_schemes (model, "mailto",
						   "http", "ftp", NULL);
	*/
	view = gnome_recent_view_bonobo_new (ui_component,
				"/menu/File/GnomeRecentDocuments/");
	gnome_recent_view_set_model (GNOME_RECENT_VIEW (view), model);

	/* destroy the model when no more views are using it */
	g_object_unref (model);

	
	/* Let's see the global history too */
	global_model = gnome_recent_model_new (GNOME_RECENT_MODEL_SORT_MRU, 20);
	global_view = gnome_recent_view_bonobo_new (ui_component,
				"/menu/Global/GnomeRecentDocuments/");
	gnome_recent_view_set_model (GNOME_RECENT_VIEW (global_view), global_model);
	g_object_unref (global_model);
	
	g_signal_connect (G_OBJECT (view), "activate",
			  G_CALLBACK (open_recent_cb), NULL);

	g_signal_connect (G_OBJECT (global_view), "activate",
			  G_CALLBACK (open_recent_cb), NULL);

	return win;
}

static gint 
delete_event_cb (GtkWidget *window, GdkEventAny *e, gpointer user_data)
{
	hello_on_menu_file_close (NULL, window, NULL);

	/* Prevent the window's destruction, since we destroyed it 
	 * ourselves with hello_app_close() */
	return TRUE;
}

static GtkWidget *
hello_new (void)
{
	GtkWidget    *global_clear;
	GtkWidget    *local_clear;
	GtkWidget    *global_limit;
	GtkWidget    *local_limit;
	GtkWidget    *content;
	GladeXML     *xml;
	BonoboWindow *win;
	gchar *tmp;

	win = hello_create_main_window();


	xml = glade_xml_new ("./gnome-recent-test.glade2", "content", NULL);

	if (!xml) {
		g_warning (_("Could not find gnome-recent-test.glade2."));
		return NULL;
	}

	content = glade_xml_get_widget (xml, "content");
	global_clear = glade_xml_get_widget (xml, "global_clear_button");
	global_limit = glade_xml_get_widget (xml, "global_limit");
	local_clear = glade_xml_get_widget (xml, "local_clear_button");
	local_limit = glade_xml_get_widget (xml, "local_limit");

	tmp = g_strdup_printf ("%d", gnome_recent_model_get_limit (model));
	gtk_entry_set_text (GTK_ENTRY (local_limit), tmp);

	/*
	tmp = g_strdup_printf ("%d", gnome_recent_model_get_limit (global_model));
	*/
	gtk_entry_set_text (GTK_ENTRY (global_limit), tmp);

	bonobo_window_set_contents (win, content);

	/* Connect to the delete_event: a close from the window manager */
	gtk_signal_connect (GTK_OBJECT (win),
			    "delete_event",
			    GTK_SIGNAL_FUNC (delete_event_cb),
			    NULL);

	g_signal_connect (G_OBJECT (global_clear), "clicked",
			  G_CALLBACK (global_clear_cb), NULL);
	g_signal_connect (G_OBJECT (local_clear), "clicked",
			  G_CALLBACK (local_clear_cb), NULL);
	g_signal_connect (G_OBJECT (global_limit), "activate",
			  G_CALLBACK (global_limit_activate_cb), NULL);
	g_signal_connect (G_OBJECT (local_limit), "activate",
			  G_CALLBACK (local_limit_activate_cb), NULL);

	return GTK_WIDGET(win);
}

int 
main (int argc, char* argv[])
{
	GtkWidget *app;

	if (!bonobo_ui_init ("bonobo-hello", "1.0", &argc, argv))
		g_error (_("Cannot init libbonoboui code"));

	app = hello_new ();

	gtk_widget_show_all (GTK_WIDGET (app));

	bonobo_main ();

	return 0;
}
