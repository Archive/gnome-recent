/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef __GNOME_RECENT_VIEW_GTK_H__
#define __GNOME_RECENT_VIEW_GTK_H__

G_BEGIN_DECLS

#define GNOME_RECENT_VIEW_GTK(obj)		G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_recent_view_gtk_get_type (), GnomeRecentViewGtk)
#define GNOME_RECENT_VIEW_GTK_CLASS(klass) 	G_TYPE_CHECK_CLASS_CAST (klass, gnome_recent_view_gtk_get_type (), GnomeRecentViewGtkClass)
#define GNOME_IS_RECENT_VIEW_GTK(obj)		G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_recent_view_gtk_get_type ())

typedef struct _GnomeRecentViewGtk GnomeRecentViewGtk;

typedef struct _GnomeRecentViewGtkClass GnomeRecentViewGtkClass;

struct _GnomeRecentViewGtkClass {
	GObjectClass parent_class;
	
	gboolean (*activate) (GnomeRecentViewGtk *view, GnomeRecentItem *item);
};

GType        gnome_recent_view_gtk_get_type (void);

GnomeRecentViewGtk * gnome_recent_view_gtk_new (GtkWidget *menu,
						const gchar *menu_key);

void gnome_recent_view_gtk_set_menu            (GnomeRecentViewGtk *view,
						GtkWidget *menu);
GtkWidget * gnome_recent_view_gtk_get_menu     (GnomeRecentViewGtk *view);


void gnome_recent_view_gtk_set_start_label     (GnomeRecentViewGtk *view,
						const gchar *label);
gchar * gnome_recent_view_gtk_get_start_label     (GnomeRecentViewGtk *view);

void gnome_recent_view_gtk_set_leading_sep     (GnomeRecentViewGtk *view,
						gboolean val);

void gnome_recent_view_gtk_set_trailing_sep    (GnomeRecentViewGtk *view,
						gboolean val);

G_END_DECLS

#endif /* __GNOME_RECENT_VIEW_GTK_H__ */
