/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/* recent-method.c - VFS module for recently used files

   Copyright (C) 2002 James Willcox  <jwillcox@cs.indiana.edu>

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

 */

#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-method.h>
#include "gnome-recent.h"


typedef struct _RecentMethod {
	GnomeRecentModel *model;

	GList *list;
	GList *current;
}RecentMethod;


static GnomeVFSResult do_open_directory  (GnomeVFSMethod                *method,
					  GnomeVFSMethodHandle         **method_handle,
					  GnomeVFSURI                   *uri,
					  GnomeVFSFileInfoOptions        options,
					  GnomeVFSContext               *context);
static GnomeVFSResult do_close_directory (GnomeVFSMethod               *method,
					  GnomeVFSMethodHandle          *method_handle,
					  GnomeVFSContext               *context);
static GnomeVFSResult do_read_directory  (GnomeVFSMethod               *method,
		                          GnomeVFSMethodHandle          *method_handle,
		                          GnomeVFSFileInfo              *file_info,
		                          GnomeVFSContext               *context);

static GnomeVFSResult do_open            (GnomeVFSMethod               *method,
                                          GnomeVFSMethodHandle         **method_handle,
                                          GnomeVFSURI                   *uri,
                                          GnomeVFSOpenMode               mode,
                                          GnomeVFSContext               *context);

static GnomeVFSResult do_open            (GnomeVFSMethod               *method,
                                          GnomeVFSMethodHandle         **method_handle,
                                          GnomeVFSURI                   *uri,
                                          GnomeVFSOpenMode               mode,
                                          GnomeVFSContext               *context)
{
	return GNOME_VFS_ERROR_NOT_SUPPORTED;	
}

static GnomeVFSResult do_close           (GnomeVFSMethod               *method,
                                          GnomeVFSMethodHandle         **method_handle,
                                          GnomeVFSContext               *context)
{
	return GNOME_VFS_ERROR_NOT_SUPPORTED;	
}

static GnomeVFSResult
do_open_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle **method_handle,
		   GnomeVFSURI *uri,
		   GnomeVFSFileInfoOptions options,
		   GnomeVFSContext *context)
{
	RecentMethod *recent;
	GList *list;

	recent = g_new (RecentMethod, 1);
	recent->model = gnome_recent_model_new (GNOME_RECENT_MODEL_SORT_MRU,
						20);

	list = gnome_recent_model_get_list (recent->model);

	if (list) {
		recent->list = list;
		recent->current = list;
	} else {
		recent->list = NULL;
		recent->current = NULL;
	}

	*method_handle = (GnomeVFSMethodHandle *)recent;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_close_directory (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSContext *context) 
{
	RecentMethod *recent = (RecentMethod *)method_handle;

	g_object_unref (recent->model);
	if (recent->list)
		g_list_free (recent->list);

	g_free (recent);

	return GNOME_VFS_OK;
}

static void
get_file_info (GnomeRecentItem *item, GnomeVFSFileInfo *file_info)
{
	gchar *uri;
	gchar *base;
	gchar *mime_type;

	uri = gnome_recent_item_get_uri (item);
	base = g_path_get_basename (uri);
	mime_type = gnome_recent_item_get_mime_type (item);
	
	file_info->name = base;
	file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_TYPE |
				  GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS |
				  GNOME_VFS_FILE_INFO_FIELDS_FLAGS |
				  GNOME_VFS_FILE_INFO_FIELDS_MTIME |
				  GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
	file_info->type = GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK;
	file_info->permissions = GNOME_VFS_PERM_USER_READ;
	file_info->flags = GNOME_VFS_FILE_FLAGS_SYMLINK;
	file_info->mtime = gnome_recent_item_get_timestamp (item);
	file_info->mime_type = mime_type;
	file_info->symlink_name = uri;
	
}

static GnomeVFSResult
do_read_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle *method_handle,
		   GnomeVFSFileInfo *file_info,
		   GnomeVFSContext *context)
{
	GnomeRecentItem *item;
	RecentMethod *recent;

	recent = (RecentMethod *)method_handle;

	if (recent->current == NULL)
		return GNOME_VFS_ERROR_EOF;
	
	item = (GnomeRecentItem *)recent->current->data;
	
	gnome_recent_item_free (item);

	get_file_info (item, file_info);

	recent->current = recent->current->next;
	
	return GNOME_VFS_OK;
}

static gboolean
do_is_local (GnomeVFSMethod *method,
	     const GnomeVFSURI *uri)
{
	return FALSE;
}

static GnomeVFSResult
do_get_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  GnomeVFSFileInfo *file_info,
		  GnomeVFSFileInfoOptions options,
		  GnomeVFSContext *context)
{
	if (gnome_vfs_uri_has_parent (uri) == 0) {
		file_info->name = g_strdup ("/");
		file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_TYPE |
					  GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->mime_type = g_strdup ("x-directory/normal");

	} else {
		gchar *str = gnome_vfs_uri_to_string (uri, 0);
		gchar *base = g_path_get_basename (str);

		file_info->name = base;
		file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_TYPE;
		file_info->type = GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK;

		g_free (str);
	}
	
	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_check_same_fs (GnomeVFSMethod *method,
                  GnomeVFSURI *a,
                  GnomeVFSURI *b,
                  gboolean *same_fs_return,
                  GnomeVFSContext *context)
{
        *same_fs_return = FALSE;

        return GNOME_VFS_OK;
}

static GnomeVFSMethod method = {
	sizeof (GnomeVFSMethod),
	do_open,
	NULL, /* create */
	do_close,
	NULL,
	NULL, /* write */
	NULL, /* seek */
	NULL, /* tell */
	NULL, /* truncate */
	do_open_directory,
	do_close_directory,
	do_read_directory,
	do_get_file_info, /* file info */
	NULL, /* info from handle */
	do_is_local,
	NULL, /* make_directory */
	NULL, /* remove directory */
	NULL, /* move */
	NULL, /* unlink */
	do_check_same_fs,
	NULL, /* set_file_info */
	NULL, /* truncate */
	NULL, /* find_directory */
	NULL  /* create_symbolic_link */
};

GnomeVFSMethod *
vfs_module_init (const char *method_name, 
		 const char *args)
{
	return &method;
}

void
vfs_module_shutdown (GnomeVFSMethod *method)
{
}
