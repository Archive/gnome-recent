
G_BEGIN_DECLS

#define GLIST_DEEP_FREE(list) g_list_foreach (list, (GFunc)g_free, NULL); \
			      g_list_free (list);


gchar * gnome_recent_util_escape_underlines (const gchar *uri);


G_END_DECLS
