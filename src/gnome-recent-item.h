
#ifndef __GNOME_RECENT_ITEM_H__
#define __GNOME_RECENT_ITEM_H__

#include <time.h>

G_BEGIN_DECLS


typedef struct _GnomeRecentItem GnomeRecentItem;

struct _GnomeRecentItem {
	gchar *uri;
	gchar *mime_type;
	time_t timestamp;

	gboolean private;

	GList *groups;
	
	GHashTable *properties;
};




/* constructors */
GnomeRecentItem * gnome_recent_item_new ();

/* automatically fetches the mime type, etc */
GnomeRecentItem * gnome_recent_item_new_from_uri (const gchar *uri);

/* deconstructor */
void gnome_recent_item_free (GnomeRecentItem *item);

GnomeRecentItem * gnome_recent_item_copy (const GnomeRecentItem *item);

void gnome_recent_item_set_uri (GnomeRecentItem *item, const gchar *uri);
gchar * gnome_recent_item_get_uri (const GnomeRecentItem *item);
gchar * gnome_recent_item_get_uri_utf8 (const GnomeRecentItem *item);

void gnome_recent_item_set_mime_type (GnomeRecentItem *item, const gchar *mime);
gchar * gnome_recent_item_get_mime_type (const GnomeRecentItem *item);

void gnome_recent_item_set_timestamp (GnomeRecentItem *item, time_t timestamp);
time_t gnome_recent_item_get_timestamp (const GnomeRecentItem *item);


/* groups */
const GList *  gnome_recent_item_get_groups (const GnomeRecentItem *item);

gboolean       gnome_recent_item_in_group (const GnomeRecentItem *item,
					   const gchar *group_name);

void           gnome_recent_item_add_group (GnomeRecentItem *item,
					    const gchar *group_name);

void           gnome_recent_item_remove_group (GnomeRecentItem *item,
					       const gchar *group_name);

void           gnome_recent_item_set_private (GnomeRecentItem *item,
					      gboolean priv);

gboolean       gnome_recent_item_get_private (const GnomeRecentItem *item);


G_END_DECLS

#endif /* __GNOME_RECENT_ITEM_H__ */
