/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef __GNOME_RECENT_STORAGE_H__
#define __GNOME_RECENT_STORAGE_H__

G_BEGIN_DECLS

#define GNOME_RECENT_STORAGE(obj)		G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_recent_storage_get_type (), GnomeRecentStorage)
#define GNOME_RECENT_STORAGE_CLASS(klass) 	G_TYPE_CHECK_CLASS_CAST (klass, gnome_recent_storage_get_type (), GnomeRecentStorageClass)
#define GNOME_IS_RECENT_STORAGE(obj)		G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_recent_storage_get_type ())

typedef struct _GnomeRecentStorage GnomeRecentStorage;

typedef struct _GnomeRecentStorageClass GnomeRecentStorageClass;

typedef struct _StorageItem StorageItem;

struct _StorageItem {
	gchar *uri;
	gchar *mime_type;
};


GType                gnome_recent_storage_get_type     (void);

/* constructors */
GnomeRecentStorage *   gnome_recent_storage_new          ();

/* public methods */
gboolean gnome_recent_storage_add      (GnomeRecentStorage *storage,
					const gchar *uri);

gboolean gnome_recent_storage_delete   (GnomeRecentStorage *storage,
					const gchar *uri);

void gnome_recent_storage_clear        (GnomeRecentStorage *storage);

GList * gnome_recent_storage_get_list  (GnomeRecentStorage *storage);

void gnome_recent_storage_changed      (GnomeRecentStorage *storage);

StorageItem * gnome_recent_storage_item_new (const gchar *uri,
					     const gchar *mime_type);

void gnome_recent_storage_item_free (StorageItem *item);

G_END_DECLS

#endif /* __GNOME_RECENT_STORAGE_H__ */
