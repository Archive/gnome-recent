/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors:
 *   James Willcox <jwillcox@cs.indiana.edu>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <glib.h>
#include <glib-object.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include "gnome-recent-storage.h"
#include "gnome-recent-util.h"

#define GNOME_RECENT_STORAGE_FILE_PATH "/.recently-used"
#define GNOME_RECENT_STORAGE_BUFFER_SIZE 1024
#define GNOME_RECENT_STORAGE_DELIMITER " "

#define GNOME_RECENT_STORAGE_MAX_LEN 500

#define GLIST_STORAGE_DEEP_FREE(list) \
	g_list_foreach (list, (GFunc)gnome_recent_storage_item_free, NULL); \
                              g_list_free (list);

struct _GnomeRecentStorage {
	GObject parent_instance;	/* We emit signals */

	GnomeVFSMonitorHandle *monitor; /* we use gnome-vfs to monitor
					 * the file
					 */

	char *path;			/* path to the file we store stuff in */
};

struct _GnomeRecentStorageClass {
	GObjectClass parent_class;
	
	void (*changed) (GnomeRecentStorage *storage, const GList *list);
};

enum {
	CHANGED,
	LAST_SIGNAL
};

static GType storage_signals[LAST_SIGNAL] = { 0 };




static gboolean
gnome_recent_storage_write_raw (GnomeRecentStorage *storage, FILE *file, const gchar *content)
{
	if (fputs (content, file) == EOF)
		return FALSE;

	return TRUE;
}

static GList *
gnome_recent_storage_delete_from_list (GList *list,
				       const gchar *uri)
{
	GList *newlist=NULL;
	GList *tmp;
	StorageItem *item;

	if (uri == NULL)
		return list;

	tmp = list;

	while (tmp) {
		item = (StorageItem *)tmp->data;
		
		if (strcmp (item->uri, uri))
			newlist = g_list_prepend (newlist, item);
		else
			gnome_recent_storage_item_free (item);

		tmp = tmp->next;
	}
	
	g_list_free (list);

	if (newlist)
		newlist = g_list_reverse (newlist);

	return newlist;
}

static gchar *
gnome_recent_storage_read_raw (GnomeRecentStorage *storage, FILE *file)
{
	GString *string;
	char buf[GNOME_RECENT_STORAGE_BUFFER_SIZE];


	g_return_val_if_fail (file != NULL, NULL);

	string = g_string_new ("");
	while (fgets (buf, GNOME_RECENT_STORAGE_BUFFER_SIZE, file)) {
		string = g_string_append (string, buf);
	}
	
	return g_string_free (string, FALSE);
}

static GList *
gnome_recent_storage_read (GnomeRecentStorage *storage, FILE *file)
{
	GList *list=NULL;
	gchar *content;
	gchar **split_content;
	gchar **split_line;
	StorageItem *item;
	int i;

	g_return_val_if_fail (file != NULL, NULL);

	content = gnome_recent_storage_read_raw (storage, file);

	split_content = g_strsplit (content, "\n", 0);

	for (i=0; split_content[i]; i++) {
		split_line = g_strsplit (split_content[i], GNOME_RECENT_STORAGE_DELIMITER, 0);

		item = gnome_recent_storage_item_new (g_strdup (split_line[0]),
						      g_strdup (split_line[1]));

		g_strfreev (split_line);

		list = g_list_prepend (list, item);
	}
	
	if (list) {
		g_strfreev (split_content);
		list = g_list_reverse (list);
	}

	return list;
}

static gboolean
gnome_recent_storage_write (GnomeRecentStorage *storage, FILE *file,
			    GList *list)
{
	GString *string;
	gchar *data;
	StorageItem *item;
	gchar *line;
	int i;
	int fd;
	int ret;
	
	g_return_val_if_fail (file != NULL, FALSE);

	fd = fileno (file);

	string = g_string_new ("");

	i=0;
	while (list) {
		item = (StorageItem *)list->data;

		if (i > 0)
			string = g_string_append (string, "\n");

		line = g_strdup_printf ("%s %s", item->uri, item->mime_type);
		string = g_string_append (string, line);
		g_strdup (line);

		list = list->next;
		i++;
	}

	data = g_string_free (string, FALSE);

	rewind (file);
	if (ftruncate (fd, 0) < 0) {
		g_warning ("Could not truncate storage file: %s\n",
				strerror (errno));
		return FALSE;
	}

	ret = gnome_recent_storage_write_raw (storage, file, data);

	g_free (data);

	return ret;
}

static FILE *
gnome_recent_storage_open (GnomeRecentStorage *storage)
{
	FILE *file;
	
	file = fopen (storage->path, "r+");
	if (file == NULL) {
		file = fopen (storage->path, "w+");

		g_return_val_if_fail (file != NULL, NULL);
	}

	return file;
}

static void
gnome_recent_storage_monitor_cb (GnomeVFSMonitorHandle *handle,
				 const gchar *monitor_uri,
				 const gchar *info_uri,
				 GnomeVFSMonitorEventType event_type,
				 gpointer user_data)
{
	GnomeRecentStorage *storage = GNOME_RECENT_STORAGE (user_data);


	switch (event_type) {
		case GNOME_VFS_MONITOR_EVENT_CHANGED:
			/* let everyone know */
			gnome_recent_storage_changed (storage);
		break;
		default:
		break;
	}
}

static void
gnome_recent_storage_finalize (GObject *object)
{
	GnomeRecentStorage *storage = GNOME_RECENT_STORAGE (object);

	g_free (storage->path);
	
	if (storage->monitor)
		gnome_vfs_monitor_cancel (storage->monitor);

}

static void
gnome_recent_storage_class_init (GnomeRecentStorageClass * klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = gnome_recent_storage_finalize;

	storage_signals[CHANGED] = g_signal_new ("changed",
			G_OBJECT_CLASS_TYPE (object_class),
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GnomeRecentStorageClass, changed),
			NULL, NULL,
			g_cclosure_marshal_VOID__POINTER,
			G_TYPE_NONE, 1,
			G_TYPE_POINTER);

	klass->changed = NULL;
}


static void
gnome_recent_storage_init (GnomeRecentStorage * storage)
{
	gchar *path;
	gchar *uri;
	GnomeVFSResult result;

	if (!gnome_vfs_init ()) {
		g_warning ("gnome-vfs initialization failed.");
		return;
	}
	
	path = g_strdup_printf ("%s" GNOME_RECENT_STORAGE_FILE_PATH,
				getenv ("HOME"));

	storage->path = path;

	uri = g_strdup_printf ("file://%s", path);

	result = gnome_vfs_monitor_add (&storage->monitor, uri,
					GNOME_VFS_MONITOR_FILE,
					gnome_recent_storage_monitor_cb,
					storage);

	if (result != GNOME_VFS_OK) {
		g_warning ("Unable to monitor file: %s",
			   gnome_vfs_result_to_string (result));
		storage->monitor = NULL;
	}
}


/**
 * gnome_recent_storage_item_new:
 *
 * This creates a new StorageItem
 *
 * Returns: a StorageItem
 */
StorageItem *
gnome_recent_storage_item_new (const gchar *uri, const gchar *mime_type)
{
	StorageItem *item;

	item = g_new (StorageItem, 1);
	item->uri = g_strdup (uri);
	item->mime_type = g_strdup (mime_type);

	return item;
}


/**
 * gnome_recent_storage_item_free:
 *
 * This destroys a StorageItem
 *
 * Returns: void
 */
void
gnome_recent_storage_item_free (StorageItem *item)
{
	g_free (item->uri);
	g_free (item->mime_type);
	g_free (item);
}

/**
 * gnome_recent_storage_new:
 *
 * This creates a new GnomeRecentStorage object.
 *
 * Returns: a GnomeRecentStorage object
 */
GnomeRecentStorage *
gnome_recent_storage_new (void)
{
	GnomeRecentStorage *storage;

	storage = GNOME_RECENT_STORAGE (g_object_new (gnome_recent_storage_get_type (), NULL));

	g_return_val_if_fail (storage, NULL);
	
	return storage;
}

/**
 * gnome_recent_storage_add:
 * @recent:  A GnomeRecentStorage object.
 * @uri: The URI you want to add to the file.
 *
 * This function adds a URI to the file of recently used URIs.
 *
 * Returns: gboolean
 */
gboolean
gnome_recent_storage_add (GnomeRecentStorage * storage, const gchar * uri)
{
	FILE *file;
	int fd;
	GList *list = NULL;
	StorageItem *item;
	gchar *mime_type;
	gboolean ret = FALSE;
	
	g_return_val_if_fail (storage != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_RECENT_STORAGE (storage), FALSE);
	g_return_val_if_fail (uri != NULL, FALSE);

	file = gnome_recent_storage_open (storage);

	g_return_val_if_fail (file != NULL, FALSE);

	fd = fileno (file);

	if (lockf (fd, F_LOCK, 0) == 0) {
		/* read existing stuff */
		list = gnome_recent_storage_read (storage, file);

		/* delete it if it's already there */
		list = gnome_recent_storage_delete_from_list (list, uri);

		/* append new stuff */
		mime_type = gnome_vfs_get_mime_type (uri);
		if (mime_type == NULL) {
			/* uh oh */
			GLIST_STORAGE_DEEP_FREE (list);
			g_warning ("Couldn't get mime type for %s.  Not adding to list.", uri);
			ret = FALSE;
			goto out;
		}
		
		item = gnome_recent_storage_item_new (uri, mime_type);
		list = g_list_prepend (list, item);

		/* if we are over the max length, trim from the end */
		list = gnome_recent_util_trim_list (list, GNOME_RECENT_STORAGE_MAX_LEN);

		/* write new stuff */
		if (!gnome_recent_storage_write (storage, file, list))
			g_warning ("Write failed: %s", strerror (errno));

		g_print ("Added %s, mime type: %s\n", uri, mime_type);

		g_free (mime_type);
		GLIST_STORAGE_DEEP_FREE (list);

		ret = TRUE;
	} else {
		g_warning ("Failed to lock:  %s", strerror (errno));
		fclose (file);
		return FALSE;
	}

out:
	if (lockf (fd, F_ULOCK, 0) < 0)
		g_warning ("Failed to unlock: %s", strerror (errno));
	
	fclose (file);

	return ret;
}



/**
 * gnome_recent_storage_delete:
 * @recent:  A GnomeRecentStorage object.
 * @uri: The URI you want to delete.
 *
 * This function deletes a URI from the file of recently used URIs.
 *
 * Returns: gboolean
 */
gboolean
gnome_recent_storage_delete (GnomeRecentStorage * storage, const gchar * uri)
{
	FILE *file;
	GList *list;
	int length;
	int fd;
	gboolean ret = FALSE;

	g_return_val_if_fail (storage != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_RECENT_STORAGE (storage), FALSE);
	g_return_val_if_fail (uri != NULL, FALSE);


	file = gnome_recent_storage_open (storage);

	g_return_val_if_fail (file != NULL, FALSE);

	fd = fileno (file);

	if (lockf (fd, F_LOCK, 0) == 0) {
		list = gnome_recent_storage_read (storage, file);

		if (list == NULL)
			goto out;

		length = g_list_length (list);

		list = gnome_recent_storage_delete_from_list (list, uri);
		
		if (length == g_list_length (list)) {
			/* nothing was deleted */
			GLIST_STORAGE_DEEP_FREE (list);
		} else {
			gnome_recent_storage_write (storage, file, list);
			GLIST_STORAGE_DEEP_FREE (list);
			ret = TRUE;
		}
	} else {
		g_warning ("Failed to lock:  %s", strerror (errno));
		fclose (file);
		return FALSE;
	}

out:
		
	if (lockf (fd, F_ULOCK, 0) < 0)
		g_warning ("Failed to unlock: %s", strerror (errno));

	fclose (file);

	return ret;
}

/**
 * gnome_recent_storage_changed:
 * @recent:  A GnomeRecentStorage object.
 *
 * This function causes a "changed" signal to be emitted.
 *
 * Returns: void
 */
void
gnome_recent_storage_changed (GnomeRecentStorage *storage)
{
	GList *list;

	list = gnome_recent_storage_get_list (storage);

	g_signal_emit (G_OBJECT (storage), storage_signals[CHANGED], 0,
			list);
}

/**
 * gnome_recent_storage_get_list:
 * @recent:  A GnomeRecentStorage object.
 *
 * This function gets the current contents of the file
 *
 * Returns: a GList
 */
GList *
gnome_recent_storage_get_list (GnomeRecentStorage *storage)
{
	FILE *file;
	GList *list=NULL;
	int fd;
	
	file = gnome_recent_storage_open (storage);

	g_return_val_if_fail (file != NULL, NULL);

	fd = fileno (file);

	if (lockf (fd, F_LOCK, 0) == 0) {
		list = gnome_recent_storage_read (storage, file);
	} else {
		g_warning ("Failed to lock:  %s", strerror (errno));
		fclose (file);
		return FALSE;
	}

	if (lockf (fd, F_ULOCK, 0) < 0)
		g_warning ("Failed to unlock: %s", strerror (errno));

	fclose (file);

	return list;
}


/**
 * gnome_recent_storage_clear:
 * @recent:  A GnomeRecentStorage object.
 *
 * This function clears the contents of the file
 *
 * Returns: void
 */
void
gnome_recent_storage_clear (GnomeRecentStorage *storage)
{
	FILE *file;
	int fd;

	file = gnome_recent_storage_open (storage);

	g_return_if_fail (file != NULL);

	fd = fileno (file);

	if (lockf (fd, F_LOCK, 0) == 0) {
		ftruncate (fd, 0);
	} else {
		g_warning ("Failed to lock:  %s", strerror (errno));
		fclose (file);
		return;
	}

	fclose (file);

	if (lockf (fd, F_ULOCK, 0) < 0)
		g_warning ("Failed to unlock: %s", strerror (errno));

}

/**
 * gnome_recent_storage_get_type:
 * @:
 *
 * This returns a GType representing a GnomeRecentStorage object.
 *
 * Returns: a GType
 */
GType
gnome_recent_storage_get_type (void)
{
	static GType gnome_recent_storage_type = 0;

	if(!gnome_recent_storage_type) {
		static const GTypeInfo gnome_recent_storage_info = {
			sizeof (GnomeRecentStorageClass),
			NULL, /* base init */
			NULL, /* base finalize */
			(GClassInitFunc)gnome_recent_storage_class_init, /* class init */
			NULL, /* class finalize */
			NULL, /* class data */
			sizeof (GnomeRecentStorage),
			0,
			(GInstanceInitFunc) gnome_recent_storage_init
		};

		gnome_recent_storage_type = g_type_register_static (G_TYPE_OBJECT,
							"GnomeRecentStorage",
							&gnome_recent_storage_info, 0);
	}

	return gnome_recent_storage_type;
}

